package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public  static  void main(String[] args){
        HashMap<String, Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach((key, value) -> {
            if (value <= 30){
                System.out.println(key + " has been added to top games list!");
                topGames.add(key);
            }
        });
        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        // Stretch Goal

        boolean addItem = true;

        Scanner userInput = new Scanner(System.in);
        while(addItem) {
            System.out.println("Would you like to add an item? Yes? or No?");
            String input = userInput.nextLine();

            if (input.isEmpty()) {
                System.out.println("No input. Please Try again.");
            } else if (input.equalsIgnoreCase("Yes")) {
                addItem = false;
                boolean addItemName = true;

                while (addItemName) {
                    System.out.println("Add Item Name");
                    String itemNameInput = userInput.nextLine();

                    if (itemNameInput.isEmpty()){
                        System.out.println("Invalid Item name cannot be Empty!");
                    } else {
                        addItemName = false;
                        boolean addItemStocks = true;
                        while (addItemStocks) {
                            int itemStocksInput = 0;
                            System.out.println("Add Number of Stocks");
                            itemStocksInput = userInput.nextInt();
                            if (itemStocksInput == 0 || itemStocksInput < 0){
                                System.out.println("Add Number of Stocks");
                            } else {
                                addItemStocks = false;
                                System.out.println(itemNameInput + " has been added with " + itemStocksInput + " stocks");
                                games.put(itemNameInput, itemStocksInput);
                                System.out.println("Available Games:");
                                System.out.println(games);
                            }
                        }
                    }
                }
            } else if (input.equalsIgnoreCase("No")) {
                System.out.println("Thank you");
                addItem = false;
            } else {
                System.out.println("Invalid input. Try again.");
            }
        }
    }
}
