package com.zuitt.example;

import java.util.Scanner;
import java.util.stream.Stream;

public class Exceptionandling {
    public static void main(String[] args){
        /*
            Excepting Handling to handle runtime errors so that normal flow of the application can be maintained an exception is an event that disrupts the normal flow of the program
        */

        Scanner input = new Scanner(System.in);
        int num = 0;
        System.out.println("Input a number:");
        try {
            num = input.nextInt();
        } catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        System.out.println("The number you entered is " + num);

        // other example

        try{
            int dividedByZero = 5/0;
            System.out.println("Rest of code in try block");
        } catch (ArithmeticException e){
            System.out.println(e);
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
